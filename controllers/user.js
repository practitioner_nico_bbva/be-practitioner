'use strict';

/**
 * Gestiona los nuevos usuarios como así también login y validación de passwords.
 */

const User = require('../db/models/user');
const { getNewToken } = require('../jwt/jwt');
const { getAuthorizationValues } = require('./security');

const createUser = (req, res) => {

  const [mail, password, user] = getAuthorizationValues(req.headers);

  const newUser = new User();
  newUser.email = mail;
  newUser.password = newUser.encryptPassword(password);
  newUser.user = user;

  newUser.save()
    .then((resSave) => {
      res.status(200).send({ message: 'Usuario creado' });
    })
    .catch((err) => {
      return {
        11000: res.status(409).send({ message: 'El mail ya existe' })
      }[err.code];
    })
}

const login = async (req, res) => {
  const [mail, password] = getAuthorizationValues(req.headers);

  //Valida el usuario
  const resUser = await findOneUser(mail);
  if (!resUser) return res.status(401).send({ message: 'Los datos ingresados no son correctos' });

  // Valida contraseña
  const isValid = await validatePSW(resUser, password);
  if (!isValid) return res.status(401).send({ message: 'Los datos ingresados no son correctos' });

  // Genera Token
  const token = getNewToken({ mail });

  res.set({
    'access-token': token,
    'access-token-type': 'jwt',
    'Content-Type': 'application/javascript',
  });

  let objResponse = {
    "code": "OK",
    "message": "Acceso de usuario completado correctamente",
    "token": token
  }

  res.status(200).send(objResponse);
}

const findOneUser = async (email) => {
  return User.findOne({ email });
}

const validatePSW = async (usr, password) => {
  const myUser = new User();
  myUser.password = usr.password;

  if (!myUser.comparePassword(password)) {
    return false;
  }
  return true;
}


module.exports = {
  createUser,
  login
}