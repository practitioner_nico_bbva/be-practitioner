'use strict';

const axios = require('axios');
const moment = require('moment');
const StationStatus = require('../db/models/stationStatus');
const StationInformation = require('../db/models/stationInformation');
const { validateTime } = require('./app');
const { URL_API, PARAMS_REQ } = require('../config/config');


/**
 * Retorna desde mongo o desde la API los valores de las estaciones.
 * Si el último llamado supera los 1 minuto, obtiene los datos desde la API.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 */
const getDataStatus = async (req, res) => {
  // Valida un tiempo determinado. Si supera dicho tiempo llama el servicio de la API, de lo contrario llama a mongo.
  const rechargeInfo = await validateTime('status', 1); //Valida 60 minutos
  const reqId = req.params.station_id;
  const stationInfo = await findOneStation(reqId);

  if (rechargeInfo) {
    try {
      let newStatus = await getStationStatus();
      newStatus = newStatus.data.data.stations;
      const currentStatus = await _mapObject(newStatus, reqId);

      try {
        res.status(200).send({ detail: { currentStatus, stationInfo } });
      } catch (err) {
        res.status(500).send({ message: err });
      }

      findOneAndUpdate(newStatus); // Guarda en la BD.

    } catch (err) {
      console.log(err);
      res.status(err.status).send({ message: err });
    }
  } else {
    try {
      let currentStatus = await findStation(reqId);
      currentStatus = formatObject(currentStatus);

      console.log(currentStatus, 'Retorna status desde BD');
      res.status(200).send({ detail: { currentStatus, stationInfo } });
    } catch (error) {
      res.status(err.status).send({ message: err });
    }
  }
}

/**
 * Busca la estación con el ID del parámetro.
 * @param {Number} id Id de la estación
 */
const findOneStation = async (id) => {
  try {
    const response = await StationInformation.findOne({ station_id: id });
    if (!response) throw new Error('Error al encontrar estación.');
    return response;
  } catch (error) {
    return error;
  }
}

/**
 * Llama al servicio de la API.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 */
const getStationStatus = async (req, res) => {
  try {
    return await axios.get(`${URL_API}/gbfs/stationStatus`, PARAMS_REQ);
  } catch (err) {
    return err;
  }
}

/**
 * Actualiza en mongo el status de cada Estación.
 * @param {Object} response Lista de estaciones
 */
const findOneAndUpdate = async (response) => {
  let allPromises = [];
  response.map((station) => {
    const query = { "station_id": station.station_id },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    allPromises.push(Promise.resolve(StationStatus.findOneAndUpdate(query, station, options)));
  });

  return await Promise.all(allPromises); //Resulve todas las promesas.
}

/**
 * Actualiza los datos.
 */
const getDataAndUpdateStatus = async() => {
  const rechargeInfo = await validateTime('status', 1); //Valida 60 minutos
  if (!rechargeInfo) return;

  let newStatus = await getStationStatus();
  newStatus = newStatus.data.data.stations;
  console.log('## Se actualizaron los datos de Status');
  return findOneAndUpdate(newStatus); // Guarda en la BD.
}

const _mapObject = async (newStatus, id) => {
  let currentStatus = null;

  newStatus.map(station => {
    if (station.station_id == id) {
      currentStatus = station;
    }
  });
  if(!currentStatus) throw new Error('Error, no se encontró en ID en el mapeo de Status.');

  return formatObject(currentStatus);
}

/**
 * Mapea el objeto que será almacenado.
 * @param {Object} obj Objeto a mapear
 */
const formatObject = (obj) => {
  const myDate = new Date(obj.last_reported * 1000).toUTCString();
  obj.lastUpdate = {
    date: moment(myDate).format("DD/MM/YYYY"),
    hour: moment(myDate).format("HH:mm:ss")
  }
  return obj;
}

/**
 * Retorna el estatus de la estación correspondiente. 
 * @param {Number} id ID de la estación
 */
const findStation = async (id) => {
  try {
    const response = await StationStatus.findOne({ station_id: id });
    if (!response) throw new Error('Error al encontrar estación.');
    return response;
  } catch (error) {
    return error;
  }
}

module.exports = {
  getStationStatus,
  getDataStatus,
  getDataAndUpdateStatus,
  findStation
}