'use strict';

/**
 * Determina los permisos del usuario.
 */

const { tokenKey, jwt } = require('../jwt/jwt');
const moment = require('moment');

/**
 * Retorna los datos del authorization en un array.
 * @param {Object} headers Header del request
 */
const getAuthorizationValues = (headers) => {
    let base64 = headers['authorization'].replace('Basic ', '');
    let decode = new Buffer.from(base64, 'base64').toString("ascii");
    return decode.split(':');
}

/**
 * Se coloca antes de los servicios que son privados.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 * @param {*} next callback
 */
const isAuth = (req, res, next) => {
    let data;
    // Verifico que tengo la cabecera
    if( !req.headers.authorization) return res.status(401).send ({ message: "No autorizado" });
    // Obtengo el token.
    const token = req.headers.authorization.replace('Basic ', '').replace('JWT ', '');
    // Si el token no existe.
    if (!token) return res.status(403).send({ message: "Es necesario el token de autenticación" });

    // Verifico el token
    jwt.verify(token, tokenKey, function (err, jwtResponse) {
        if (err) {
            console.log(`ERROR No autorizado:  ${err}`);
            return res.status(401).send({ message: 'Token inválido' });
        }

        data = jwtResponse;
    });
    // Si data no existe retorno error.
    if(!data) return new Error();

    // Verifico el tiempo de expiración.
    if( data.expiresIn <= moment().unix()) return res.status(401).send({ message: "El token ha expirado" }); 
    // Asigno el mail a req para utilziarlo en los demás métodos. 
    req.mail = data.mail;

    next();
}

module.exports = {
    getAuthorizationValues,
    isAuth
}