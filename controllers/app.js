const moment = require('moment');

let lastUpdate = {
    info: undefined,
    status: undefined
}

/**
 * Valida que se haya cumplido el lapso determinado desde la última ejecución del servicio.
 * @param {String} element El nombre del servicio
 * @param {Number} min Cantidad de tiempo que debe transcurrir.
 */
const validateTime = async (element, min = 1) => {
    const currentTime = new moment();
    let lastUpdateElement = lastUpdate[element];
    
    if (!lastUpdateElement) {
        lastUpdate[element] = new moment();
        return true;
    }
    
    var duration = currentTime.diff(lastUpdateElement, 'minutes');
    if (duration > min) {
        lastUpdate[element] = new moment();
        return true;
    }
    return false;
}


module.exports = {
    validateTime
}