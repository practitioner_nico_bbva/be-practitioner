'use strict';

/**
 * Gestiona la información de las estaciones.
 */

const axios = require('axios');
const StationInformation = require('../db/models/stationInformation');
const { getDataAndUpdateStatus } = require('./stationStatus');
const { validateTime } = require('./app');
const { URL_API, PARAMS_REQ } = require('../config/config');

/**
 * Retorna desde mongo o desde la API los valores de las estaciones.
 * Si el último llamado supera los 60 minutos, obtiene los datos desde la API.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 */
const getDataInformation = async (req, res) => {
  // Valida un tiempo determinado. Si supera dicho tiempo llama el servicio de la API, de lo contrario llama a mongo.
  const rechargeInfo = await validateTime('info', 60); //Valida 60 minutos

  getDataAndUpdateStatus();

  if (rechargeInfo) {
    try {
      const newStations = await stationInformation();
      res.status(200).send({ detail: newStations });
      findOneAndUpdate(newStations); // Guarda en la BD.
    } catch (err) {
      console.log(err);
      res.status(err.status).send({ message: err });
    }
  } else {
    try {
      const newStations = await getFromDB();
      console.log('Retorna desde BD');
      res.status(200).send({ detail: newStations });
    } catch (error) {
      res.status(err.status).send({ message: err });
    }
  }

}

/**
 * Realiza el llamado del servicio. Retorna un objeto mapeado.
 */
const stationInformation = async () => {
  try {
    const response = await axios.get(`${URL_API}/gbfs/stationInformation`, PARAMS_REQ);
    return _mapObject(response.data.data.stations);
  }
  catch (error) {
    return error
  }
}

/**
 * Actualiza los datos.
 */
const getDataAndUpdateInformation = async () => {
  // Valida un tiempo determinado. Si supera dicho tiempo llama el servicio de la API, de lo contrario llama a mongo.
  const rechargeInfo = await validateTime('info', 60); //Valida 60 minutos
  if (!rechargeInfo) return;

  const newStations = await stationInformation();
  console.log('## Se actualizaron los datos de Information')
  return findOneAndUpdate(newStations); // Guarda en la BD.
}

const getFromDB = async () => {
  return StationInformation.find();
}

// Busca cada uno y lo actualiza.
const findOneAndUpdate = async (response) => {
  let allPromises = [];
  response.map((station) => {
    const query = { "station_id": station.station_id },
      options = { upsert: true, new: true, setDefaultsOnInsert: true };

    allPromises.push(Promise.resolve(StationInformation.findOneAndUpdate(query, station, options)));
  });

  return await Promise.all(allPromises); //Resulve todas las promesas.
}

/**
 * Busca la estación con el ID del parámetro.
 * @param {Number} id Id de la estación
 */
const findOneStation = async (id) => {
  try {
    const response = await StationInformation.findOne({ station_id: id });
    if (!response) throw new Error('Error al encontrar estación.');
    return response;
  } catch (error) {
    return error;
  }
}

/**
 * Retorna un objeto mapeado.
 * @param {Object} stations Estaciones
 */
const _mapObject = async (stations) => {
  let newStations = [];
  stations.map(station => {
    let mappedStation = {
      station_id: station.station_id,
      description: station.name,
      geolocation: {
        latitude: station.lat,
        longitude: station.lon
      },
      icon: './resources/images/atm.svg',
      altitude: station.altitude,
      address: station.address,
      post_code: station.post_code,
      capacity: station.capacity,
      rental_methods: station.rental_methods,
      groups: station.groups
    }

    mappedStation.icon = station.capacity < 15 ? './resources/images/atm.svg' : './resources/images/atm-large.svg'

    newStations.push(mappedStation);
  });

  return newStations;
}

module.exports = {
  stationInformation,
  getFromDB,
  findOneStation,
  getDataInformation,
  getDataAndUpdateInformation
}