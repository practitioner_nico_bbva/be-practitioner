'use strict';

/**
 * Controlador de "recordatorios". 
 */


const User = require('../db/models/user');
const StationInfo = require('./stationInformation');


/**
 * Obtiene los recordatorios de un usuario específico.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 */
const getSchedule = async (req, res) => {
  try {
    const resolve = await User.findOne({ "email": req.mail });
    if (!resolve) return res.status(500).send({ message: 'No se encuentra el usuario' });
    console.log('Retorna Schedules : ' + resolve.schedule);
    res.status(200).send({ data: resolve.schedule });
  } catch (err) {
    return res.status(500).send({ message: 'Error al Obtener recordatorio' });
  }
}

/**
 * Almacena un recordatorio de un suario específico.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 */
const saveSchedule = async (req, res) => {
  const query = { "email": req.mail };
  let scheduleToSave = req.body;
  scheduleToSave.id = Date.now().toString();

  const { description, address } = await StationInfo.findOneStation(req.body.stationId);
  scheduleToSave.description = description;
  scheduleToSave.address = address;

  try {
    const resolve = await User.updateOne(query, { $push: { schedule: scheduleToSave } });
    console.log('resolve', resolve);
    if (resolve.nModified < 1) return res.status(500).send({ message: 'No se encuentra el usuario' });
  } catch (err) {
    return res.status(500).send({ message: 'Error al guardar recordatorio' });
  }

  res.status(200).send({ message: 'Se guardó recordatorio', data: scheduleToSave });
}

/**
 * Elimina el recordatorio de un usuario.
 * @param {*} req Request del servicio
 * @param {*} res Response del servicio.
 */
const deleteSchedule = async (req, res) => {
  const query = { "email": req.mail };

  try {
    let resolve = await User.findOne({ "email": req.mail });
    console.log('object', resolve.schedule)
    resolve = await User.updateOne( query, { $pull: { schedule: { id: req.params.id_schedule } } } );
    if (!resolve) return res.status(500).send({ message: 'No se encuentra el usuario' });
    resolve = await User.findOne({ "email": req.mail });
    return res.status(200).send({ data: resolve.schedule });
  } catch (err) {
    return res.status(500).send({ message: 'Error al Obtener recordatorio' });
  }
}

module.exports = {
  getSchedule,
  saveSchedule,
  deleteSchedule
}