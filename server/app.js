const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const morgan = require('morgan');
const { isAuth } = require('../controllers/security');

// IMPORT ROUTS
const auth = require('../routes/public/auth');
const bicycle_routes = require('../routes/public/bicycle');
const schedule_routes = require('../routes/private/schedule');

// CONFIG APP
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

// MIDDLEWARE
app.use(function (req, res, next) { next(); });
app.use(morgan('dev'));

// USE ROUTES
app.get('/', (req, res) => res.status(200).send('<h1>Bienvenido a la API de ECOBICI</h1>'));
app.use('/auth', auth);
app.use('/bicycle', bicycle_routes);
// Rutas privadas.
app.use('/user', isAuth, schedule_routes );

module.exports = {
  app
}