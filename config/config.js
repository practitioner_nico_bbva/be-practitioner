const PORT = process.env.PORT || 1122;

// Utilizar localhost para desarrollo, para el contenedor, utilizar mongo.
//"localhost" || "mongo";
const URL_DB = 'mongodb://mongo:27017/bicycle';
const URL_API = "https://apitransporte.buenosaires.gob.ar/ecobici";

// Datos personales.
const PARAMS_REQ = {
  params: {
    client_id: "0fcf04a9249547c28f6a435e9e9ff783",
    client_secret: "FcCbECb28514499F83cD69E9e121d362"
  }
};

module.exports = {
  PORT,
  URL_DB,
  URL_API,
  PARAMS_REQ
}