'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const userSchema = Schema({
    email: { type: String, unique: true, lowercase: true },
    password: { type: String },
    user: { type: String },
    signupDate: { type: Date, default: Date.now() },
    lastLogin: Date,
    schedule: Array
});

userSchema.methods.encryptPassword = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};

userSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('users', userSchema);