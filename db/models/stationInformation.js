' use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const stationInformationSchema = Schema({
    station_id: { type: String, unique: true, lowercase: true },
    description: String,
    geolocation: {
        latitude: Number,
        longitude: Number
    },
    icon: String,
    iconOptions: {
        standard: String,
        large: String
    },
    altitude: Number,
    address: String,
    post_code: String,
    capacity: Number,
    rental_methods: Array,
    groups: Array

});

module.exports = mongoose.model('stationInformations', stationInformationSchema);