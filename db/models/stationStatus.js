
'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const stationStatusSchema = Schema({
    station_id: String,
    lastUpdate: Object,
    num_bikes_available: Number,
    num_bikes_available_types: {
        mechanical: Number,
        ebike: Number
    },
    num_bikes_disabled: Number,
    num_docks_available: Number,
    num_docks_disabled: Number,
    is_installed: Number,
    is_renting: Number,
    is_returning: Number,
    last_reported: Number,
    is_charging_station: Boolean,
    status: String
});

module.exports = mongoose.model('stationStatus', stationStatusSchema);