const mongoose = require('mongoose');
const  { URL_DB } = require('../config/config');

mongoose.connect(URL_DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})
    .then(() => console.log('Base de datos conectada a bicycle en el puerto 27017'))
    .catch((err) => console.error(err));