'use strict'

const jwt = require('jsonwebtoken');
const tokenKey = "PrAct1710n3r";

const getNewToken = (data) => {
    return jwt.sign(data, tokenKey, {
        expiresIn: '1d'
    });
}

module.exports = {
    getNewToken,
    tokenKey, 
    jwt
}