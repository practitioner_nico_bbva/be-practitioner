const { app } = require('./server/app');
const { getDataAndUpdateInformation } = require('./controllers/stationInformation');
const { getDataAndUpdateStatus } = require('./controllers/stationStatus');
const { PORT } = require('./config/config');

// Init
require('./db/database');
require('./controllers/app');

//Guarda en la base de datos po primera vez.
(async () => {
    console.log('Iniciando datos');
    try {
        getDataAndUpdateStatus();
        getDataAndUpdateInformation();
    } catch (err) {
        throw new Error('No se pueden obtener los datos de la API');
    }
})()


// INICIAR SERVER
// ==============================================
app.listen(PORT);
console.log('Server iniciado en el puerto ' + PORT);
console.log(`http://localhost:${PORT}`);