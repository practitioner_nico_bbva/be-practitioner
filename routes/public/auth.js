'use strict';

const express = require('express');
const router = express.Router();
const { createUser, login } = require('../../controllers/user');

router.post('/users/access', login);
router.post('/users/signup', createUser); 

module.exports = router;

