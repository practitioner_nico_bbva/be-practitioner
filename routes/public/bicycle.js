const express = require('express');
const router = express.Router();
const { getDataInformation } = require('../../controllers/stationInformation');
const { getDataStatus } = require('../../controllers/stationStatus');


router.get('/', function (req, res) {
  res.send('<h1>Puedas consultar a la API de CABA ecobici.</h1>');
});

router.get('/stationStatus/:station_id', getDataStatus);
router.get('/stationInformation', getDataInformation);

module.exports = router;