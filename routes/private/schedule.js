const express = require('express');
const router = express.Router();
const  { getSchedule,  saveSchedule, deleteSchedule } = require('../../controllers/schedule');

router.get('/', function (req, res) {
  res.send('<h1>Puedas manipular los recordatorios de un usuario.</h1>');
});

router.get('/schedule', getSchedule);
router.post('/schedule', saveSchedule);
router.delete('/schedule/:id_schedule', deleteSchedule);

module.exports = router;