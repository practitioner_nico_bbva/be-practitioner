# API ECOBICI

Es una api que combina el llamado a una api del gobierno de la ciudad, con la conexión a una base de datos mongo.

Para no utilizar demasiado la API de l gobierno, se utiliza un siste de cache, en donde verifica que haya transcurrido un tiempo determinado desde la última ver que se invocó el servicio.

- Para la información de la estación se evalua un tiempo de 60 minutos, porque no son datos que se modifiquen.
- Para el status de la estación se evalua un tiempo de 1 minuto, porque estos datos se actualizan todo el tiempo.

## Interfaces


### **Públicas**

>### Crear usuario | _POST_
> endpoint: **'/users/signup'**
>> REQUEST  
>>> HEADER  
**authorization**:  _[mail, password, user]_ convertido en base 64. 
>> RESPONSE  
>>> BODY  
**message**: 'Usuario creado'

>### Iniciar sesión | _POST_
 > **'/users/access'** 
>> REQUEST  
>>> HEADER  
**authorization**:  _[mail, password]_ convertido en base 64.
>> RESPONSE  
>>> HEADER  
**access-token**: token

>### Obtener información de las estaciones | _GET_
> endpoint: **'/stationInformation'**
>> RESPONSE  
**data**: ``` { "_id" : ObjectId("5d692bd98690bd0bde8a19b6"), "station_id" : "2", "__v" : 0, "address" : "Ramos Mejia, Jose Maria, Dr. Av. & Del Libertador Av.", "altitude" : 0, "capacity" : 20, "description" : "002 - Retiro I", "geolocation" : { "latitude" : -34.5924233, "longitude" : -58.3747151 }, "groups" : [ "RETIRO" ], "icon" : "./resources/images/atm-large.svg", "post_code" : "11111", "rental_methods" : [ "KEY", "TRANSITCARD", "PHONE" ] } ```


>### Obtener status de una estación | _GET_
> endpoint: **'/stationStatus/:station_id'**
>> REQUEST  
>>>PARAMS  
:station_id _String_
>> RESPONSE  
**data**: ``` { "_id" : ObjectId("5d68781181e19c464c751d5e"), "station_id" : "3", "__v" : 0, "is_charging_station" : false, "is_installed" : 1, "is_renting" : 1, "is_returning" : 1, "last_reported" : 1567383225, "num_bikes_available" : 0, "num_bikes_available_types" : { "mechanical" : 0, "ebike" : 0 }, "num_bikes_disabled" : 0, "num_docks_available" : 20, "num_docks_disabled" : 0, "status" : "IN_SERVICE" } ```




___

### **Privadas**
>### Obtener los recordatorios de un usuario | _GET_
> endpoint: **'/schedule'**
>> REQUEST  
>>>HEADER  
**authorization**: token  
>> RESPONSE    
**data**: ``` [ { "hour" : 4, "minutes" : 0, "stationId" : 87, "id" : "1567373409258", "description" : "087 - Guayaquil", "address" : "Guayaquil & Doblas" }, { "hour" : 8, "minutes" : 11, "stationId" : 87, "id" : "1567373415686", "description" : "087 - Guayaquil", "address" : "Guayaquil & Doblas" } ] ```


>### Agregar un recordatorio a un usuario | _POST__
> endpoint: **'/schedule'**
>> REQUEST  
>>>HEADER  
**authorization**: token  
>>>BODY  
``` { "hour" : 8, "minutes" : 11, "stationId" : 87 } ```
>> RESPONSE  
**message**: ``` Se guardó recordatorio ```

>### Eliminar un recordatorio a un usuario | _DELETE__
> endpoint: **'/schedule/:id_schedule'**
>> REQUEST  
>>>HEADER  
**authorization**: token  
>>>PARAMS  
_id_schedule_
>> RESPONSE  
**message**: ``` Se eliminó recordatorio ```


## JWT
Utiliza segurazión por medio de JWT.  
En la cabecera **authorization**:  _mail:password_ convertido en base 64.

## Instalación obligatoria
1. Descargar la imagen de mongo  
> $ docker pull mongo  
2. Crear una red llamada **ecobici**  
> $ docker network create ecobici
3. Verificar que la red se encuentre, nos ayudamos con grep  
> $docker network ls | grep ecobici  
3. Ejecutar MONGO en la red **ecobici**  
> $ sudo docker run --name mongo --network ecobici --rm -p 27017:27017 mongo

## Instalación local  
Dirigirse a **config/config.js** y modificar _mongo_, por _localhost_.
> $ npm install  

Ejecución  
> $ node index.js

## Instalación con Docker  

1. descargar la imagen de ecobici-api  
> $ docker pull nicoc123/ecobici-api

2. Ejecutar la API en la red ecobici  
> $ sudo docker run -p 1122:1122 --network ecobici nicoc123/ecobici-api

## Acceso
> http://localhost:1122